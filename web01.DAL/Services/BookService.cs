using Dapper;
using MySqlConnector;
using web01.DAL.Models;

namespace web01.DAL.Services;

public class BookService : IBookService
{
  private string connString;
  private MySqlConnection conn;

  public BookService(string connString)
  {
    this.connString = connString;
    this.conn = new MySqlConnection(this.connString);
  }

  public IEnumerable<Book> GetBookList()
  {
    var ret = this.conn.Query<Book>("select * from Book order by rand() limit 10");
    return ret;
  }
}