using web01.DAL.Models;

namespace web01.DAL.Services;

public interface IBookService
{
  public IEnumerable<Book> GetBookList();
}