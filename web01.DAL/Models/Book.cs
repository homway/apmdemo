namespace web01.DAL.Models;

public class Book
{
  public int BookId { get; set; }
  public string? BookName { get; set; }

}