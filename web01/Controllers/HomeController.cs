﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using web01.DAL.Services;
using web01.DAL.Models;
using web01.Models;

namespace web01.Controllers;

public class HomeController : Controller
{
  private readonly ILogger<HomeController> _logger;
  private readonly IConfiguration configuration;

  public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
  {
    _logger = logger;
    this.configuration = configuration;
  }

  public IActionResult Index()
  {
    return View();
  }

  public IActionResult Privacy()
  {
    return View();
  }

  [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
  public IActionResult Error()
  {
    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
  }

  public IActionResult Throw()
  {
    throw new Exception("custom error...");
  }

  public IActionResult BookList()
  {
    BookService svc = new BookService(this.configuration.GetConnectionString("devConnectionString"));
    IList<Book> ret = svc.GetBookList().ToList();
    return new JsonResult(ret);
  }
}
