﻿// See https://aka.ms/new-console-template for more information
using System.Reflection;
using DbUp;
using Microsoft.Extensions.Configuration;

IConfiguration config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

var connectionString = config.GetConnectionString("devConnectionString");
var upgrader = DeployChanges.To
      .MySqlDatabase(connectionString)
      .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
      .LogToConsole()
      .Build();

var result = upgrader.PerformUpgrade();

if (!result.Successful)
{
  Console.ForegroundColor = ConsoleColor.Red;
  Console.WriteLine(result.Error);
  Console.ResetColor();
  Console.ReadLine();
  return -1;
}

Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Success!");
Console.ResetColor();
return 0;