.PHONY: build

up:
	docker-compose up -d

down:
	docker-compose down

migration:
	dotnet run --project web01.Migration/web01.Migration.csproj

build:
	dotnet build web01.Migration
	dotnet build web01.DAL
	dotnet build web01

run:
	dotnet run --project web01/web01.csproj

watch:
	dotnet watch run --project web01/web01.csproj

siege:
	siege -c 100 -r 200 -d1 'http://localhost:5000/home/booklist'

scanner:
	cd web01; \
	dotnet sonarscanner begin /k:"web01" /d:sonar.host.url="http://localhost:9000"  /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c"; \
	dotnet build; \
	dotnet sonarscanner end /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c";

	cd web01.DAL; \
	dotnet sonarscanner begin /k:"web01.DAL" /d:sonar.host.url="http://localhost:9000"  /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c"; \
	dotnet build; \
	dotnet sonarscanner end /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c";

	cd web01.Migration; \
	dotnet sonarscanner begin /k:"web01.Migration" /d:sonar.host.url="http://localhost:9000"  /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c"; \
	dotnet build; \
	dotnet sonarscanner end /d:sonar.login="53c72857a0e68115ff0bc904ef252509335c593c";

clean:
	rm -rf web01/.sonarqube web01/bin web01/obj
	rm -rf web01.DAL/.sonarqube web01.DAL/bin web01.DAL/obj
	rm -rf web01.Migration/.sonarqube web01.Migration/bin web01.Migration/obj
